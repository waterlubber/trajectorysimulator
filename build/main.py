# The main.py script allows for manually testing specific results or optimizing code automatically.
# It relied on the pre-compiled Cython trajectorySimulator binary:
import trajectorySimulator as ts

optimize = False # Choose whether or not to generate a new optimal result based on hard-coded parameters:
if optimize:
    results = ts.optimize()
    parameters = results['x']
    print("System Parameters: \n{}\n".format(parameters))
else:   # Enter a target rocket to simulate.
    # Parameters are all in SI.
    # Launch Angle (from horizontal); Prop Mass for Stages 1, 2, 3; TWR Initial for Stages 1, 2, 3
    parameters = [8.92961440e+01, 1.05935869e+04, 4.56673699e+03, 7.89127711e+02, 2.49871582e+00, 1.32684188e+00, 1.12853812e+00, 1.53944031e+01, 7.54410795e+01]
ts.printResultInfo(parameters)


