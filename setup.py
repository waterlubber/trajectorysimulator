from distutils.core import setup
from setuptools.config import read_configuration
from Cython.Build import cythonize

conf_dict = read_configuration('setup.cfg')
ext_options = {"compiler_directives": {"profile": True}, "annotate": True}
setup(
    ext_modules=cythonize("src/*.pyx", **ext_options)
)
