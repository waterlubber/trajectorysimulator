from libc.math cimport sqrt, atan2, pi
## I wrote this "library" to assist in 2D vector math. It mostly works with tuples and is designed to be very fast (much faster than numpy) for the sake of dealing with small vectors.

# Add two vectors
cdef inline (double, double) addVector((double, double) v1, (double, double) v2):
    return v1[0]+v2[0], v1[1]+v2[1]


# Subtract a vector from another
cdef inline (double, double) subVector((double, double) v1, (double, double) v2):
    return v1[0] - v2[0], v1[1] - v2[1]


# Multiply a vector by a scalar
cdef inline (double, double) sProduct((double, double) v1, double s):
    return s*v1[0], s*v1[1]


# Divide a vector by a scalar
cdef inline (double, double) sQuotient((double, double) v1, double s):
    return v1[0]/s, v1[1]/s


# Return the dot product of two 2D vectors
cdef inline double dotProduct((double, double) v1,(double, double) v2):
    return v1[0] * v2[0] + v1[1] * v2[1]


# Return the magnitude of the cross product of two 2D vectors, i.e AB sin Θ
cdef inline double flatCrossProduct((double, double) v1,(double, double) v2): # Calculate the z-axis cross product of two 2D vectors
    return v1[0]*v2[1] - v1[1]*v2[0]


# Normalize a vector.
cdef inline (double, double) getDirection((double, double) v1):
    cdef double magnitude = sqrt(v1[0]*v1[0] + v1[1]*v1[1])  # Return a direction vector
    return v1[0] / magnitude, v1[1] / magnitude


# Calculate the magnitude of a vector.
cdef inline double fastNorm((double, double) vector): # A function for quickly computing the magnitude of a vector
    return sqrt(vector[0]*vector[0] + vector[1]*vector[1])  # Return a direction vector


# Calculate the magnitude of a vector, squared. (or just square the vector...)
cdef inline double magSquared((double, double) vector): # Return magnitude of a vector squared
    return vector[0]*vector[0] + vector[1]*vector[1]


# Calculate the angle of a vector
cdef inline double getAngle((double, double) vector):
    return atan2(vector[1], vector[0]) * 180 / pi