from libc.math cimport log

## This file mostly serves to house the RocketStage class, a key component of the trajectory simulator.
# The RocketStage handles much of the "rocket equation" work for the stage, and generally provides a complete picture
# of the stage as a function of time. It computes deltaV, masses, thrusts, etc. RocketStages are created for each new
# attempt at an optimization, which allows the program to dynamically compute many of the variables from a small set of configuration constants.

# Global configuration constants.

DEF g = 9.80665  # in m/s^2
DEF earthRadius = 6371000  # Earth radius in meters
DEF mu = 3.986004418e14  # μ, the GM product, for earth
DEF earthSurfaceV = 407.74  # Tangential velocity at surface of Earth


# Calculate the air density at the current position of the stage. This should really be in another file, but it resides here for now as a squatter.
cdef double getAirDensity(double altitude):
    # From https://www.spaceacademy.net.au/watch/debris/atmosmod.htm
    return 1.3 ** (-1.0 * altitude / 7000)  # Result in kg/m^3


# A RocketStage is a class that represents all the values for one stage of a rocket
# The thrust, TWR, and burntime arguments are all mutually exclusive
cdef class RocketStage:
    def __cinit__(self, double isp, double mass_prop, double mass_flat, # Required arguments: Specific impulse, propellant mass, and structural mass (incl. subsequent stages).
                  double thrust=0, double twr=0, double burntime = 0, # Mutually exclusive "thrust-setting" arguments. They are processed to compute actual thrust.
                  double drag_coefficient=0.2, double area=2, # Drag coefficients. Area is in m^2.
                  double startupDelay=0, double struct_coefficient=0, double mass_stuff=0, int stageNumber=0): # Delay before stage ignition, structural coefficient, "miscellaneous fixed mass".
        # Ensure that mutually exclusive burntime arguments are not used
        if (burntime and thrust) or (burntime and twr) or (thrust and burntime):
            raise Exception("Conflicting arguments: both thrust and TWR set")

        # Component Mass Calculation
        self.massPropellant = mass_prop  # The mass of the propellant in kg
        self.massStuff = mass_stuff # The mass of fairings, etc. in kg. mass_flat is more designed for subsequent stages, and this provides a place to count a fixed value as structural mass
        self.massDry = mass_flat + mass_prop * struct_coefficient / (1 - struct_coefficient) + mass_stuff # The total structural mass of the stage and its payload
        self.massStructural = mass_prop * struct_coefficient / (1 - struct_coefficient) + mass_stuff # The total structural mass of the stage alone
        self.massTotal = self.massDry + self.massPropellant # The total mass of the rocket at the stage

        # DeltaV and "Rocket Equation" Calculation
        self.isp = isp  # Specific impulse in seconds
        self.stageDV = isp * g * log((self.massTotal / self.massDry))

        # Thrust and Burntime processing and calculation
        if burntime: # If a burntime is given
            self.burnTime = burntime
            self.thrust = mass_prop * isp * g / burntime # kg m/s^2 = kg * m/s^2 * s / s
            self.massFlow = mass_prop / burntime  # Mass flow in kg/s
        else: # If a burntime is not given
            self.thrust = thrust + twr * self.massTotal * g # Calculate thrust via TWR or flat thrust
            self.massFlow = self.thrust / isp / g  # Mass flow in kg/s
            self.burnTime = self.massPropellant / self.massFlow

        # Calculate drag helper values
        self.dragCoefficient = drag_coefficient
        self.crossArea = area  # Cross sectional area
        self.dragFactor = drag_coefficient * area

        # Calculate parameters for early shutdown.
        self.separationTime = 0 # The time the stage is "released" from the previous one.
        self.startupDelay = startupDelay # The time until the engine ignites.

        # Calculate TWR
        self.TWRInitial = self.thrust / g / self.massTotal
        self.TWRFinal = self.thrust / g / self.massDry
        # Index of the stage
        self.stageNumber = stageNumber

    cdef double getDrag(self, double altitude, double velocity): # Magnitude of the current drag force on the stage
        return getAirDensity(altitude) * self.dragFactor * velocity * velocity * 0.5  # Calculate drag via 1/2 * ρ V^2 A C_d

    cdef double getMass(self, double time):
        # Calculate the current mass of the stage.
        # This is equal to the starting mass, subtracted by the amount of propellant that has burned
        # Additionally, ensure that the "burn time" is not negative -- this would *add* mass to the stage.
        return self.massTotal - self.massFlow * max(0, time - self.getStageIgnitionTime())

    cdef double getBurnTimeRemaining(self, double time):
        # Calculate remaining burn time.
        # The remaining burn time is equal to the burn time subtracted by the time the engine has been burning.
        # The engine has burned for a time equal to the difference between the start time and now.
        return self.burnTime - (time - self.getStageIgnitionTime())

    cdef double getStageIgnitionTime(self):
        # Get the absolute time of the stage's ignition. Requires a correctly set separationTime (which is set during a simulation)
        return self.separationTime + self.startupDelay

    cdef double getDeltaVRemaining(self, double time):
        # Calculate the remaining DeltaV of a stage at a given time.
        return self.isp * g * log((self.getMass(time) / self.massDry))
