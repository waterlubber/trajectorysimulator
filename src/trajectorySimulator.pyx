from vectorMath cimport *
from orbitHelpers cimport *
from classes cimport RocketStage
from libc.math cimport sin, cos, pi, atan2
from datetime import datetime
import numpy as np
import scipy.optimize
import matplotlib.pyplot as pypl

# Constants: adjust these to convert the program to freedom (imperial) units
DEF mu = 3.986004418e14  # μ, the GM product, for Earth, in metric units
DEF earthRadius = 6371000 # Radius of earth in m
DEF earthSurfaceV = 407.74 # Tangential Velocity at Launch Site in m/s
DEF ihat = (1, 0) # Special tuple vectors used for some calculations
DEF jhat = (0, 1)
DEF metric = False # Print metric units
DEF customary = True # Print customary units (all calculations are done in SI)
# Conversion factors from metric to imperial
DEF kg_lbs = 2.204623 # 1 kg in lbs
DEF m_ft = 3.28084 # 1 m in ft
DEF km_nmi =  0.53995680345572 # 1 km in nmi
DEF N_lbf = 0.22480894387096 # 1 N in pounds force

# Threshold and cheaty global variable for early termination of simulation
DEF threshold = 0.25
goodFitness = False

# Calculate the new position after a timestep via a vectorization of d = 1/2 at^2 + v_0 t
cdef (double, double) calculatePosition(double dt,(double, double) last_p,(double, double) last_v,(double, double) acceleration):
    return addVector(last_p, addVector(sProduct(last_v, dt), sProduct(acceleration, 0.5 * dt * dt)))


# Calculate the new velocity after a timestep
cdef (double, double) calculateVelocity(double dt,(double, double) last_v,(double, double) last_a,(double, double) acceleration):
    return addVector(last_v, sProduct(addVector(last_a, acceleration), dt / 2))

# Calculate the force of gravity at a given altitude
cdef double getGravity(double altitude):
    return mu / (altitude + earthRadius) / (altitude + earthRadius)


# Write information on a stage by appending it to the iInfo list.
# This also grabs the DeltaV of the stage and adds it.
# This is an awful function, and really just a debugging interface that morphed into a UI.
cdef (double) printStage(RocketStage stage, list iInfo):
    iInfo.append("---- Stage {} Info ----".format(stage.stageNumber))
    if metric:
        iInfo.append("Burntime: {:<+7.1f}s\tSep Times: {:.1f} - {:.1f} s\tΔV: {:.1f} m/s\tTWRi: {:.2f}\tTWRf: {:.2f}\tThrust: {:.2f} N".format(stage.burnTime, stage.getStageIgnitionTime(), stage.burnTime + stage.separationTime, stage.stageDV, stage.TWRInitial, stage.TWRFinal, stage.thrust))
        iInfo.append("Propellant Mass: {:.1f} kg\tStructural Mass: {:.1f} kg\tDry Mass: {:.1f} kg\tTotal Mass: {:.1f} kg".format(stage.massPropellant, stage.massStructural, stage.massDry, stage.massTotal))
    if customary:
        iInfo.append("Burntime: {:<+7.1f}s\tSep Times: {:.1f} - {:.1f} s\tΔV: {:.1f} ft/s\tTWRi: {:.2f}\tTWRf: {:.2f}\tThrust: {:.2f} lbf".format(stage.burnTime, stage.getStageIgnitionTime(), stage.burnTime + stage.separationTime, stage.stageDV*m_ft, stage.TWRInitial, stage.TWRFinal, stage.thrust*N_lbf))
        iInfo.append("Propellant Mass: {:.1f} lbm\tStructural Mass: {:.1f} lbm\tDry Mass: {:.1f} lbm\tTotal Mass: {:.1f} lbm".format(stage.massPropellant*kg_lbs, stage.massStructural*kg_lbs, stage.massDry*kg_lbs, stage.massTotal*kg_lbs))
    return stage.stageDV


# Stages is a list of Stage objects to iterate over. InitialXVelocity is an initial x velocity applied to the very first stage to initiate the gravity turn.
# dt is the time interval (in seconds) between calculations
cdef computeTrajectory(list stages, double launch_angle,double boost_time,double dt, bint printData=False):
    # The simulation stores the values in the trajectory computation
    # The format is [time, position, velocity]
    cdef double time = 0.0 # Stores global time for simulation
    cdef double stageSepTime = 0.0 # Stores a time of a stage separation
    cdef (double, double) position = (0.0, 0.0) # Position vector (rotating reference frame)
    cdef (double, double) lastPosition = (0.0, 0.0) # Last position vector from previous iteration
    cdef (double, double) velocity = (cos(launch_angle * pi / 180), sin(launch_angle * pi / 180))
    cdef (double, double) lastVelocity = (cos(launch_angle * pi / 180), sin(launch_angle * pi / 180))
    cdef (double, double) lastAcceleration = (0.0, 9.81)
    # Determine the the initial direction before starting a gravity turn
    cdef (double, double) setDirection = getDirection((cos(launch_angle * pi / 180), sin(launch_angle * pi / 180)))
    cdef (double, double) force
    cdef int stageIndex = 0  # Set the starting stage to the first one
    # Variables for storing times, altitudes, and longitudes for each stage (for graphing during print)
    cdef list times = []
    cdef list altitudes = []
    cdef list longitudes = []
    cdef list drags = []
    cdef list angles = []
    cdef RocketStage currentStage
    stageIndex = 0 #testing something
    iterationInfo = []
    while True:
        # Update currentStage to point to the current stage we're simulating
        currentStage = stages[stageIndex]
        # Staging logic: if a stage is out of propellant, move on to the next stage
        if currentStage.getBurnTimeRemaining(time) < dt:
            # Eject the previous stage
            stageIndex += 1
            # If we have now run out of stages, end the simulation:
            if stageIndex >= len(stages):
                if printData:
                    deltaVTotal = 0 # Reset the "total stage delta v" counter...TODO: move this to someplace sensible
                    for stage in stages:
                        deltaVTotal += printStage(stage, iterationInfo)
                    # Plot data
                    plotData(times, longitudes, altitudes, drags, angles)
                    iterationInfo.append("\n---- Final Trajectory Information ----")
                    if metric:
                        iterationInfo.append("Time: {:.1f}\tAltitude: {:.2f} km\tVelocity: {:0.1f} m/s Θ = {:0.1f}°\tTotal ΔV: {:0.1f} m/s".format(time, position[1]/1000, fastNorm(velocity), atan2(velocity[1], velocity[0]) * 180 / pi, deltaVTotal))
                    if customary:
                        iterationInfo.append("Time: {:.1f}\tAltitude: {:.2f} nmi\tVelocity: {:0.1f} ft/s Θ = {:0.1f}°\tTotal ΔV: {:0.1f} ft/s".format(time, position[1]/1000*km_nmi, fastNorm(velocity)*m_ft, atan2(velocity[1], velocity[0]) * 180 / pi, deltaVTotal*m_ft))
                return time, position, velocity, iterationInfo
            # Update the current stage and set its ignition time to now
            currentStage = stages[stageIndex]
            currentStage.separationTime = time

        # Calculate the forces on the rocket
        # Thrust Calculation: Uses direction and thrust, low sensitivity to errors
        # This gives us the average force over dt
        if time < currentStage.getStageIgnitionTime(): # If the stage has not been "ignited" yet...
            force = 0, 0 # Don't apply any thrust!
        elif time <= boost_time: # If we're beginning our gravity turn
            force = sProduct(setDirection, currentStage.thrust) # Force thrust along a specific vector
        else: # Otherwise, we're under normal conditions -- thrust prograde
            force = sProduct(getDirection(velocity), currentStage.thrust)
        # Gravity Calculation
        force = subVector(force, sProduct(jhat, currentStage.getMass(time + dt) * getGravity(altitude=position[1])))
        # Drag Calculation: Might be helped by using the "new" velocity
        force = subVector(force, sProduct(getDirection(velocity), currentStage.getDrag(altitude=position[1], velocity=fastNorm(velocity))))
        # Centrifugal Force Correction
        force = addVector(force, sProduct(jhat, (velocity[0]+earthSurfaceV) * (velocity[0]+earthSurfaceV) / (position[1]+earthRadius) * currentStage.getMass(time + dt)))

        # Divide the force by mass to calculate the average acceleration over period dt
        acceleration = sQuotient(force, currentStage.getMass(time))
        # Implement the velocity Verlet algorithm, at the suggestion of Kyle K. from ERPL (thanks!)
        # https://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet
        position = calculatePosition(dt, lastPosition, lastVelocity, acceleration)
        velocity = calculateVelocity(dt, lastVelocity, lastAcceleration, acceleration)
        lastPosition = position[0], position[1] # This one weird trick is somehow faster!
        lastVelocity = velocity[0], velocity[1]
        lastAcceleration = acceleration[0], acceleration[1]

        # Save data for charts
        if printData:
            times.append(time)
            longitudes.append(position[0]/1000)
            altitudes.append(position[1]/1000)
            drags.append(currentStage.getDrag(altitude=position[1], velocity=fastNorm(velocity)))
            angles.append(90-getAngle(velocity))

        # print("Time: {:.1f}\tAcceleration: {:.2f} {:.2f}\tAltitude: {:.0f}\tVelocity: {:0.1f} m/s Θ = {:0.1f}°".format(time, acceleration[0],acceleration[1], position[1], fastNorm(velocity), np.arctan2(velocity[1],velocity[0])*180/pi))
        time += dt


# Cross Sectional Area: 15.9ft^2 = 1.5m^2
# Payload Mass: 226kg, assuming 250kg for misc flight control hardware, etc.
cdef runComputation(double launch_angle, double m1, double m2, double m3, double t1, double t2, double t3, double d2=0, double d3=0, bint printData=False):
    # Generate three RocketStage objects with our desired characteristics
    cdef RocketStage stage3 = RocketStage(twr=t3, isp=287, mass_prop=m3, mass_flat=500/kg_lbs, struct_coefficient=0.118714,
                                          drag_coefficient=0.2, area=1.267, startupDelay=d3, stageNumber=3, mass_stuff=63.8/kg_lbs) #3-P
    cdef RocketStage stage2 = RocketStage(twr=t2, isp=297, mass_prop=m2, mass_flat=stage3.massTotal, struct_coefficient=0.090167,
                                          drag_coefficient=0.2, area=1.267, startupDelay=d2, stageNumber=2, mass_stuff=(91.9 + 122.8 + 42.6)/kg_lbs) #2-3, F_P, F2-3
    cdef RocketStage stage1 = RocketStage(twr=t1, isp=274, mass_prop=m1, mass_flat=stage2.massTotal, struct_coefficient=0.092277,
                                          drag_coefficient=0.2, area=1.267, stageNumber=1, mass_stuff=(220.8 + 42.6)/kg_lbs) # 1-2, F1-2
    cdef list stageList = [stage1, stage2, stage3]
    # Run a trajectory computation and collect data from it
    cdef double trajectoryTime
    cdef (double, double) trajectoryPosition, trajectoryVelocity
    cdef list iterationInfo, times, longitudes, altitudes, drags
    trajectoryTime, trajectoryPosition, trajectoryVelocity, iterationInfo = computeTrajectory(stageList, launch_angle, 0, 0.01, printData)  # Hold a pitch of 88° for 10 seconds
    # Calculate the final angle after third stage ECO
    finalTheta = atan2(trajectoryVelocity[1], trajectoryVelocity[0]) * 180 / pi
    # Convert to inertial reference frame centered at core of earth (dropping altitude & adding Earth's rotational speed)
    trajectoryVelocity = (trajectoryVelocity[0]+earthSurfaceV,trajectoryVelocity[1])
    trajectoryPosition = (0, trajectoryPosition[1] + earthRadius)

    # Calculate orbital parameters
    ecc = getEccentricity(trajectoryPosition, trajectoryVelocity)
    sma = getSemiMajorAxis(trajectoryPosition[1], trajectoryVelocity)
    periapsis = getPeriapsis(sma, ecc)
    if printData:
        iterationInfo.append("\n---- Final Orbit Info ----")
        if metric:
            iterationInfo.append("Ecc: {:.3f}\tSMA: {:.2f} km \tPerigee: {:.2f} km \tApogee: {:.2f} km ".format(ecc, sma / 1000, periapsis / 1000, getApoapsis(sma, ecc) / 1000))
        if customary:
            iterationInfo.append("Ecc: {:.3f}\tSMA: {:.2f} nmi\tPerigee: {:.2f} nmi\tApogee: {:.2f} nmi".format(ecc, sma / 1000 * km_nmi, periapsis / 1000 *km_nmi, getApoapsis(sma, ecc) / 1000*km_nmi))
    return finalTheta, iterationInfo, trajectoryVelocity, periapsis, getApoapsis(sma, ecc), sma, ecc


startTime = datetime.now()


# Function for printing plots of recorded simulation data
def plotData(times, longitudes, altitudes, drags, angles):
    # Convert values into numpy arrays
    x1 = np.asarray(times)
    x2 = np.asarray(longitudes) / 1.852
    y = np.asarray(altitudes) / 1.852
    y2 = np.asarray(drags) * N_lbf
    y3 = np.asarray(angles)
    # Plot Altitude/Time
    fig, ax = pypl.subplots()
    ax.plot(x1, y, label="Altitude", linewidth=2.0)
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Altitude (nmi)')
    ax.set_title("Altitude over Time")
    fig.show()
    # Plot Altitude/Position
    fig, ax = pypl.subplots()
    ax.plot(x2, y, label="Altitude", linewidth=2.0)
    ax.set_xlabel('Position (nmi)')
    ax.set_ylabel('Altitude (nmi)')
    ax.set_title("Altitude over Position")
    fig.show()
    # Plot Drag/Time
    fig, ax = pypl.subplots()
    ax.plot(x1, y2, label="Drag", linewidth=2.0)
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Drag (lbf)')
    ax.set_title("Drag over Time")
    fig.show()
    # Plot Angle/Time
    fig, ax = pypl.subplots()
    ax.plot(x1, y3, label="Angle", linewidth=2.0)
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Angle (°)')
    ax.set_title("Angle over Time")
    fig.show()


# Wrapper function for computation: runs a trajectory calculation and returns a fitness based on the closeness
# of the final orbit to the target orbit.
cpdef optimizationStep(x):
    # x is an array of the variables that we need
    cdef double launchAngle, m1, m2, m3, t1, t2, t3, d1, d2, finalTheta, periapsis, aopaosis, sma, ecc
    cdef list iterationInfo
    cdef (double, double) trajectoryVelocity
    launchAngle, m1, m2, m3, t1, t2, t3, d1, d2 = x
    finalTheta, iterationInfo, trajectoryVelocity, periapsis, apoapsis, sma, ecc = runComputation(launchAngle, m1, m2, m3, t1, t2, t3, d2=d1, d3=d2)
    #print("Perigee Attempt: {:.1f}km".format(periapsis))
    #return -1*periapsis # Optimize for highest semimajor axis, penalizing high eccentricities.
    fitness = fastNorm(((150 - periapsis/1000*km_nmi), (150 - apoapsis/1000*km_nmi)))
    if fitness < threshold:
        goodFitness = True
    return fitness


# Print detailed information for a specified set of rocket parameters (produced by the optimizer)
# This effectively runs one (1) simulation and stores detailed results from it for user output
def printResultInfo(x):
    launchAngle, m1, m2, m3, t1, t2, t3, d1, d2 = x # Load in the various parameters
    finalTheta, iterationInfo, trajectoryVelocity, periapsis, apoapsis, sma, ecc = runComputation(launchAngle, m1, m2, m3, t1, t2, t3, d2=d1, d3=d2, printData=True) # Run a simulation (in verbose mode)
    print("- - - Simulation Complete - - -")
    print("The following parameters were used:")
    print("Launch Theta: {:.1f}°\t\tStage Masses 1: {:.1f} kg 2: {:.1f} kg 3: {:.1f} kg\nStage TWRs: {:.3f} {:.3f} {:.3f}\tIgnition Delays: 2: {:.2f} s 3: {:.2f} s\n".format(launchAngle, m1, m2, m3, t1, t2, t3, d1, d2))
    for info in iterationInfo:
        print(info)
    if metric:
        print("Final Theta: {}\tVelocity: {} m/s\tPeriapsis: {:.2f} km\tApoapsis: {:.2f} km\tEcc: {:.3f}".format(finalTheta, trajectoryVelocity, periapsis/1000, apoapsis/1000,ecc))
    if customary:
        print("Final Theta: {}\tVelocity: {} ft/s\tPeriapsis: {:.2f} nmi\tApoapsis: {:.2f} nmi\tEcc: {:.3f}".format(finalTheta, sProduct(trajectoryVelocity,m_ft), periapsis/1000*km_nmi, apoapsis/1000*km_nmi,ecc))


# Called by the scipy.optimize function to update us on its progress
cpdef progressCallback(x, convergence):
    cdef double launchAngle, m1, m2, m3, t1, t2, t3, d1, d2
    launchAngle, m1, m2, m3, t1, t2, t3, d1, d2 = x
    if metric:
        print("PROGRESS: {:.1f}%\tTheta:{:.1f}\tPropellant Masses (kg): {:.1f}, {:.1f}, {:.1f}\tTWRs: {:.1f}, {:.1f}, {:.2f}\tDelays: {:.1f} s {:.1f} s".format(convergence*100, launchAngle, m1, m2, m3, t1, t2, t3, d1, d2))
    if customary:
        print("PROGRESS: {:.1f}%\tTheta:{:.1f}\tPropellant Masses (lbm): {:.1f}, {:.1f}, {:.1f}\tTWRs: {:.1f}, {:.1f}, {:.2f}\tDelays: {:.1f} s {:.1f} s".format(convergence*100, launchAngle, m1*kg_lbs, m2*kg_lbs, m3*kg_lbs, t1, t2, t3, d1, d2))
    if goodFitness:
        print('beans')
    return goodFitness

# Run an optimization
def optimize():
    # Bounds for computation - constrain max total propellant mass as well as values for each stage
    propellantMassConstraint = scipy.optimize.LinearConstraint(np.array([0, 1, 1, 1, 0, 0, 0, 0, 0]), lb=15000, ub=15950) # Ensure sum of propellant masses is reasonable
    bounds = [(70, 89.95), (10590, 12355), (4230, 4935.6), (694, 810), (1.4, 2.5), (0.8, 3), (0.1, 1.7), (0, 120), (0, 120)]
    print("Beginning Optimization")
    #result = scipy.optimize.minimize(optimizationStep, x0=initial, method='COBYLA', constraints=propellantMassConstraint, bounds=bounds, options={'maxiter': 1000, 'disp': True, 'rhobeg': 0.1})
    result = scipy.optimize.differential_evolution(optimizationStep, strategy='best1bin', bounds=bounds, workers=-1, constraints=propellantMassConstraint,
                                                   mutation=(0.4, 0.9), popsize=30, callback=progressCallback, maxiter=700, disp=True, polish=False, tol=0.05)
    return result


if __name__ == "__main__":
    print(optimize())