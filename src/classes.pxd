cdef class RocketStage:
    # These are simply duplicates of the definitions in classes.pyx.
    # They are necessary for Cython reasons.
    cdef double isp
    cdef double massStuff
    cdef double massPropellant
    cdef double massStructural
    cdef double massDry
    cdef double massTotal
    cdef double thrust
    cdef double massFlow
    cdef double dragCoefficient
    cdef double crossArea
    cdef double stageDV
    cdef double dragFactor
    cdef double separationTime
    cdef double startupDelay
    cdef double burnTime
    cdef double TWRInitial
    cdef double TWRFinal
    cdef int stageNumber
    cdef double getDrag(self, double altitude, double velocity)
    cdef double getMass(self, double time)
    cdef double getBurnTimeRemaining(self, double time)
    cdef double getStageIgnitionTime(self)
    cdef double getDeltaVRemaining(self, double time)
