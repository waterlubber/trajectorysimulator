## The OrbitHelpers file contains a few functions to help with orbital mechanics.

from vectorMath cimport *
from libc.math cimport sqrt

DEF mu = 3.986004418e14  # μ, the GM product, for Earth, in metric units
DEF earthRadius = 6371000 # Radius of Earth in m


# Calculate the magnitude of the eccentricty of the orbit using the position and velocity vectors from the inertial reference framme
cdef inline double getEccentricity((double, double) position, (double, double) velocity):
    cdef double h2 = flatCrossProduct(position, velocity)**2
    return sqrt(1+(2*h2*getOrbitalEnergy(position[1], velocity)/mu**2))

# Calculate the orbital energy (E) using an altitude and velocity
cdef inline double getOrbitalEnergy(double altitude, (double, double) velocity):
    return 0.5 * magSquared(velocity) - mu / altitude


# Calculate the semi-major axis of an orbit with altitude (from earth core) and velocity
cdef inline double getSemiMajorAxis(double altitude, (double, double) velocity):
    return -mu/2/getOrbitalEnergy(altitude, velocity)


# Calculate a periapsis and return its height above the earth (perigee)
cdef inline double getPeriapsis(double sma,double ecc):
    return (sma * (1 - ecc)) - earthRadius


cdef inline double getApoapsis(double sma,double ecc):
    return (sma * (1 + ecc)) - earthRadius